import { createStyles } from "@mui/styles";

const styles = () =>
  createStyles({
    rootTitle: {
      margin: 0,
      position: "relative",
      paddingLeft: 24,
      paddingRight: 24,
      paddingTop: 0,
      paddingBottom: 16,
    },
    closeButton: {
      position: "absolute",
      right: 8,
      top: 0,
      padding: 0,
      "&:hover": {
        backgroundColor: "transparent",
      },
    },
  });

export { styles };
