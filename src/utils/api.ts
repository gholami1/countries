import Axios from "axios";

class Api {
  _axios;
  constructor() {
    this._axios = Axios.create();
    this._axios.defaults.baseURL = `https://restcountries.com`;
    this._axios.defaults.timeout = 15000;
    this._axios.defaults.headers.common.Accept = "application/json";
    this._axios.defaults.headers.common["Content-Type"] = "application/json";
  }

  async get(url: string, params = {}) {
    return this._axios
      .get(url, {
        params,
      })
      .catch((err) => {
        throw err.response;
      });
  }
}

const api = new Api();
export default api;
