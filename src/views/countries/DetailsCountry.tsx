import { createStyles, makeStyles } from "@mui/styles";

const useStyles = makeStyles(() =>
  createStyles({
    imgBox: {
      width: 100,
      height: 100,
    },
  })
);

interface PropsTypes {
  details: Record<any, any>;
}

const DetailsCountry = ({ details }: PropsTypes) => {
  const classes = useStyles();

  return (
    <>
      <img src={details.flag} className={classes.imgBox} />
      <p>name : {details.name}</p>
      <p>nativeName : {details.nativeName}</p>
    </>
  );
};

export default DetailsCountry;
