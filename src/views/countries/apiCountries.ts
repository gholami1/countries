import api from "../../utils/api";

export const getCountries = async () => {
  const url = `/v2/all`;
  try {
    return await api.get(url);
  } catch (err) {
    throw err;
  }
};
