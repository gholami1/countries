import { useEffect, useState } from "react";
import { Grid, Container, CircularProgress, Pagination } from "@mui/material";
import { makeStyles } from "@mui/styles";

import { chunkArrayInGroups } from "../../helper/chunkArray";
import DetailsCountry from "./DetailsCountry";
import { getCountries } from "./apiCountries";
import Modal from "../../components/modal";
import { styles } from "./style";

const useStyles = makeStyles(() => styles());

interface ICountries {
  data: any[];
  load: boolean;
}

interface IDataModal {
  countriy: Record<any, any>;
  open: boolean;
}

const Countries = () => {
  const classes = useStyles();
  const [countries, setCountries] = useState<ICountries>({
    data: [],
    load: true,
  });
  const [dataModal, setDataModal] = useState<IDataModal>({
    countriy: {},
    open: false,
  });
  const [pageInfo, setPageInfo] = useState({
    current: 1,
    total: 5,
    size: 12,
  });
  const [data, setData] = useState<any>([]);

  useEffect(() => {
    (async () => {
      const { data } = await getCountries();
      setCountries((prevState) => ({
        ...prevState,
        data,
        load: false,
      }));

      setPageInfo((prevState) => ({
        ...prevState,
        total: Math.ceil(data.length / pageInfo.size),
      }));

      const contriesChunk = chunkArrayInGroups(data, pageInfo.size);
      setData(contriesChunk);
    })();
  }, []);

  const handleModal = (countriy: object) => {
    setDataModal((prevState) => ({
      ...prevState,
      countriy,
      open: true,
    }));
  };

  const handleCloseModal = () => {
    setDataModal((prevState) => ({
      ...prevState,
      countriy: {},
      open: false,
    }));
  };

  const handleChange = (e: React.ChangeEvent<unknown>, page: number) => {
    setPageInfo((prevState) => ({
      ...prevState,
      current: page,
    }));
  };

  return (
    <>
      {countries.load === true ? (
        <div className={classes.progressBox}>
          <CircularProgress color="inherit" />
        </div>
      ) : (
        <Container maxWidth="lg" className={classes.root}>
          <Grid container>
            {data?.length > 0 &&
              data[pageInfo.current - 1]?.map(
                (countriy: Record<any, any>, index: number) => {
                  return (
                    <Grid
                      item
                      md={3}
                      sm={6}
                      xs={12}
                      key={index}
                      onClick={() => handleModal(countriy)}
                    >
                      <Grid className={classes.boxItem}>
                        <img src={countriy?.flag} className={classes.imgBox} />
                        <p>{countriy.name}</p>
                      </Grid>
                    </Grid>
                  );
                }
              )}
          </Grid>
          <Grid container className={classes.pageBox}>
            <Pagination
              variant="outlined"
              color="primary"
              page={pageInfo.current}
              count={pageInfo.total}
              onChange={(e, page) => handleChange(e, page)}
            />
          </Grid>
        </Container>
      )}
      <Modal
        open={dataModal.open}
        onClose={handleCloseModal}
        className={classes.modalBox}
      >
        <DetailsCountry details={dataModal.countriy} />
      </Modal>
    </>
  );
};

export default Countries;
