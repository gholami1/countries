import { createStyles } from "@mui/styles";

const styles = () =>
  createStyles({
    root: {
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
      alignItems: "center",
      textAlign: "center",
      padding: "50px 0",
    },
    boxItem: {
      borderRadius: 8,
      padding: 15,
      margin: 15,
      height: 150,
      border: "dashed 1.5px #5A2583",
    },
    imgBox: {
      width: 50,
      height: 50,
      borderRadius: 8,
    },
    pageBox: {
      padding: 20,
      justifyContent: "center",
      "& .MuiPagination-root": {
        direction: "initial",
      },
    },
    modalBox: {
      "& .MuiDialogTitle-root": {
        direction: "initial",
      },
      "& .MuiDialogContent-root": {
        borderTop: "unset",
        textAlign: "center",
      },
    },
    progressBox: {
      justifyContent: "center",
      display: "flex",
      alignItems: "center",
      height: "100vh",
    },
  });

export { styles };
