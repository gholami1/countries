import type { NextPage } from "next";
import Countries from "../src/views/countries";

const CountriesPage: NextPage = () => {
  return (
    <div>
      <Countries />
    </div>
  );
};

export default CountriesPage;
